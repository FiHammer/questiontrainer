#!/bin/python3
import json
import random
import os
import sys
from typing import List


class S:
	def __init__(self, s) -> None:
		self.str = s
		self.correct = None
		self.id = -1
		self.shuffleId = -1

	def __eq__(self, o: object) -> bool:
		return self.str == o

	def __ne__(self, o: object) -> bool:
		return self.str != o

	def __repr__(self):
		return self.str

	def __str__(self):
		return self.str


def clear():
	if sys.platform == "linux":
		os.system("clear")
	elif sys.platform == "win32":
		os.system("cls")


def read():
	with open("data.json") as file:
		return json.load(file)


def write(data):
	with open("data.json", "w") as file:
		json.dump(data, file)


def ansIdsToStr(ans):
	s = ""
	for a in ans:
		s += str(a + 1) + " and "
	return s.rstrip(" and ")


def ansToStr(ans: List[S]):
	s = ""
	for a in ans:
		if a.correct:
			s += str(a.shuffleId + 1) + " and "
	return s.rstrip(" and ")


def getInput():
	try:
		i = input("Correct answer: ")
	except KeyboardInterrupt as e:
		if getYes("Really end?"):
			raise e
		else:
			return getInput()
	
	o = []
	for part in i.split(","):  # multiple spearated with a comma
		try:
			o.append(int(part) - 1)
		except ValueError:
			return getInput()
	return o


def getYes(x):
	try:
		i = input(x + ": ").lower()
	except KeyboardInterrupt:
		print("")
		return False
	
	if i == "y" or i == "true" or i == "yes":
		return True
	else:
		return False


def sameAnswers(answer_ids: List[int], answers: List[S]) -> bool:
	one_correct = False
	for answer in answers:
		if not answer.correct:
			continue
		else:
			one_correct = True
		if answers.index(answer) not in answer_ids:
			return False

	if not one_correct:
		return False

	return True


def main(data, shuffle=True):
	corrects = 0
	wrongs = 0

	ids = [str(x) for x in range(1, data["now_id"])]

	while True:  # mainloop
		if shuffle:
			random.shuffle(ids)

		id_ = ids[0]

		answers = []
		for x in range(len(data[id_]["answers"])):
			s = S(data[id_]["answers"][x])
			s.id = x
			answers.append(s)

		for correct_id in data[id_]["answer"]:
			if correct_id < 0:  # ignore them
				continue

			answers[correct_id].correct = True  # is correct
		random.shuffle(answers)

		for x in range(len(answers)):
			answers[x].shuffleId = x

		clear()
		print("Correct: %s - Wrong: %s\n" % (corrects, wrongs))

		print("Question (%s - left: %s):\n" % (id_, len(ids)) + data[id_]["question"])
		for answer in answers:
			index = answers.index(answer) + 1
			print("\t%s. - " % index + answer.str)

		user_answer = getInput()

		print("")
		if sameAnswers(user_answer, answers):
			print("Correct")
			corrects += 1
			ids.remove(id_)
		else:
			print("Incorrect: %s\nCorrect: %s" % (ansIdsToStr(user_answer), ansToStr(answers)))
			print("Explanation: " + data[id_]["explanation"])
			if getYes("Overwrite data?"):
				new_ids = []
				for user_id in user_answer:
					for answer in answers:
						if answer.shuffleId == user_id:
							new_ids.append(answer.id)

				data[id_]["answer"] = new_ids
				print("Overwritten!")
				wrongs -= 1
				corrects += 1

			wrongs += 1
		try:
			input()
		except KeyboardInterrupt:
			pass  # do nothing


if __name__ == "__main__":
	shuffle_ = getYes("shuffle?")

	data_ = read()
	try:
		main(data_, shuffle_)
	except KeyboardInterrupt:
		pass
	
	write(data_)
