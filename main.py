import os
import pydub
from pydub.playback import play
import simpleaudio
import glob
import pyaudio
from typing import List


def getValidMusicFiles(x: List[str]) -> List[str]:
    items = []
    for filter_ in x:
        items += glob.glob(filter_)
    return items


def main():
    waves = getValidMusicFiles(["*.wav"])
    mp3s = getValidMusicFiles(["*.mp3"])
    mkvs = getValidMusicFiles(["*.mkv"])
    flacs = getValidMusicFiles(["*.flac"])

    wave_obj = simpleaudio.WaveObject.from_wave_file(waves[0])
    # play_obj = wave_obj.play()
    # play_obj.wait_done()

    x_mp3 = pydub.AudioSegment.from_file(flacs[0], format="flac")

    p = simpleaudio.play_buffer(
        x_mp3.raw_data,
        num_channels=x_mp3.channels,
        bytes_per_sample=x_mp3.sample_width,
        sample_rate=x_mp3.frame_rate
    )

    print(p.is_playing())

    p.wait_done()


if __name__ == '__main__':
    main()
